
public class ArrayDemo2 {

	public static final int NUM_ELEMENTS = 10;
	public static void main(String[] args) {
		int[] array = new int[NUM_ELEMENTS];
		
		for(int i = 0; i < array.length; i++) {
			array[i] = (i + 1) * 100;
		}
		for(int i = 0; i < array.length; i++) {
			System.out.println(array[i]);			
		}		
	}
}
