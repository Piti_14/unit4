
public class Matrix10x10 {
	
	public static final int DIMENSION = 10;
	private static int[][] matrix = new int[DIMENSION][DIMENSION];

	public static void main(String[] args) {
		
		createMatrix();
		changeMatrix();
		printMatrix();
		System.out.println(howManyEmptyRows(matrix));
		System.out.println(howManyEmptyColumns(matrix));

	}

	private static void printMatrix() {
		for(int i = 0; i <= matrix.length - 1; i++) {
			for(int j = 0; j <= matrix[0].length - 1; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static void changeMatrix() {
		matrix[0][4] = 1;
		matrix[2][6] = 1;
		matrix[3][1] = 1;
		matrix[8][6] = 1;
		
	}

	private static void createMatrix() {
		for(int i = 0; i <= matrix.length - 1; i++) {
			for(int j = 0; j <= matrix[0].length - 1; j++) {
				matrix[i][j] = 0;
			}
		}	
	}
	
	private static String howManyEmptyRows(int[][] m) {
		
		boolean validRow = true;
		int counter = 0;
		for(int i = 0; i <= m.length - 1; i++) {
			for(int j = 0; j <= (m[0].length - 1); j++) {
				if(m[i][j] != 0) {
					validRow = false;
					break;
				}
			}
			if (validRow == true) {
				counter++;
			}
			validRow = true;
		}
		return "\nThere are " + counter + " empty rows in this matrix." ;
	} 
	
	private static String howManyEmptyColumns(int[][] m) {
		
		boolean validCol = true;
		int counter = 0;
		for(int i = 0; i <= m.length - 1; i++) {
			for(int j = 0; j <= m[0].length - 1; j++) {
				if(m[j][i] != 0) {
					validCol = false;
					break;
				}
			}
			if (validCol == true) {
				counter++;
			}
			validCol = true;
		}
		return "There are " + counter + " empty columns in this matrix." ;
	}
}
