import java.util.Scanner;

public class ArrayDemo4 {

	public static void main(String[] args) {
		int x, y;
		Scanner value = new Scanner(System.in);
		System.out.println("Enter the number of rows");
		x = value.nextInt();
		
		System.out.println("Enter the number of columns");
		y = value.nextInt();
		value.close();
		
		int[][] matrix = new int[x][y];
		int counter = 1;
		
		for(int i = 0; i < x; i++) {
			for(int j = 0; j < y; j++) {
				matrix[i][j] = counter;
				counter ++;
			}
		}
		printMatrix(matrix);
	}
	
	public static void printMatrix( int[][] m)  {
		for(int i = 0; i < m.length; i++) {
			System.out.print(m[i][0]);
			for(int j = 1; j < m[0].length; j++) {
				System.out.print(", " + m[i][j]);
			}
			System.out.println();
		}
	}
}
