
public class ArrayDemo3 {

	public static void main(String[] args) {
		int[] array = {10, 20, 101, 45, 64, 890, 63, 123, 5, 7125};
		
		System.out.print("{" + array[0]);
		for(int i = 1; i < array.length; i++) {
			System.out.print(", " + array[i]);
		}
		System.out.print("}");
	}

}
