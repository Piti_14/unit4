import java.util.Scanner;

public class BattleShip {
	
		public static final int DIMENSION = 8;
		public static final int NUMSHIPS = 10;
		static int missingShips = 10;
		static boolean gameOver;
		static char[][] matrix = new char [DIMENSION][DIMENSION];

	public static void main(String[] args) {
		
		char letter;
		int number;
		Scanner value = new Scanner(System.in);
		gameOver = false;
		
		createMatrix();
		printMatrix();
		addShipsToMatrix();
		
		while(!gameOver) {
			System.out.println("Enter a letter(Row): ");
			letter = value.next().toUpperCase().charAt(0);
			
			System.out.println("\nEnter a number(Column): ");
			number = value.nextInt();
			
			shoot(letter, number);
			
			if(missingShips == 0) {
				gameOver = true;
			}
		}
		value.close();
		System.out.println("You have drowned all the ships. Congratulations!!");

	}
	
	private static void shoot(char letter, int number) {
		int row = letter - 'A'; //ascii conversion
		int col = number - 1;
		boolean hit;
		
		if(matrix[row][col] != 'S') {
			matrix[row][col] = 'O';
			hit = false;
		} else {
			matrix[row][col] = 'X';
			hit = true;
		}
		if(hit) {
			missingShips--;
		}
		printMatrix();
		
	}

	public static void printHeader() {
		System.out.print("  ");
		for(int i = 1; i <= matrix[0].length; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
	
	public static void createMatrix() {
		for(int row = 0; row < matrix.length; row++) {
			for(int col = 0; col < matrix[0].length; col++) {
				matrix[row][col] = '·';
			}
		}	
	}	

	public static void addShipsToMatrix() {
		int randomRow, randomCol;
		int ships = 0;
		while(ships < NUMSHIPS) {
			randomRow = (int) (Math.random() * DIMENSION);
			randomCol = (int) (Math.random() * DIMENSION);
			
			if(matrix[randomRow][randomCol] != 'S') {
				matrix[randomRow][randomCol] = 'S';
				ships++;
			}
		}
	} 

	public static void printMatrix() {
		printHeader();
		char verticalHeader = 'A';
		for(int row = 0; row < matrix.length; row++) {
			System.out.print(verticalHeader + " ");
			verticalHeader++;
			for(int col = 0; col < matrix[0].length; col++) {
				if(matrix[row][col] == '·') {
				System.out.print(matrix[row][col] + " ");
				} else if(matrix[row][col] == 'S'){
					System.out.print('·' + " ");
				} else {
					System.out.print(matrix[row][col] + " ");
				}
			}
			System.out.println();
		}	
	}
}
