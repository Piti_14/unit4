import java.util.Scanner;

public class BreakDemo {

	public static void main(String[] args) {
		int n;
		Scanner value = new Scanner(System.in);
		boolean prime = true;
		
		System.out.println("Enter a number to see if is prime");
		n = value.nextInt();
		value.close();
		
		for(int i = 2; i < n; i++) {
			if(n%i == 0) {
				prime = false;
				break;
			}
		}
		if(prime) {
			System.out.println(n + " is a prime number");
		} else {
			System.out.println(n + " is not a prime number");
		}
	}
}
