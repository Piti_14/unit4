import java.util.Scanner;

public class InverseOrder {

	public static void main(String[] args) {
		int x;
		Scanner value = new Scanner(System.in);
		int[] array = new int[10];
		
		for(int i = 1; i <= 10; i++) { // better to set i to (length -1) and do i-- !!!
			System.out.print("Enter number " + i +": ");
			x = value.nextInt();
			
			array[array.length - i] = x;
		}
		value.close();
		System.out.print("\nThe inverted order of those numbers:\n{" + array[0]);
		for(int i = 1; i < array.length; i++) {
			System.out.print(", " + array[i]);
		}
		System.out.print("}");
	}
}
