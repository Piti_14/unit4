import java.util.Scanner;

public class letterDNI {

	public static void main(String[] args) {
		char[] letters = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S',
				'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
		Scanner value = new Scanner(System.in);
		int x;
		
		System.out.println("Please enter your DNI number: ");
		x = value.nextInt();
		value.close();
		
		int remainder = x % 23;
		System.out.println("Your DNI letter is: " + letters[remainder]);
	}
}
