import java.util.Scanner;

public class GreaterToSmaller {

	public static void main(String[] args) {
		int x;
		//boolean biggest;
		Scanner value = new Scanner(System.in);
		int[] array = new int[10];
		
		for(int i = 1; i <= 10; i++) {
			System.out.print("Enter number " + i +": ");
			x = value.nextInt();
			
			array[array.length - i] = x;
		}
		value.close();
		System.out.print("\nThe inverted order of those numbers:\n{" + array[0]);
		for(int i = 1; i < array.length; i++) {
			System.out.print(", " + array[i]);
		}
		System.out.print("}");
	}
}
